# Photo Triage

A simple tool for doing image preview and triaging

* Launch from CLI in folder that needs processing
    * Starts a local flask dev server that can be viewed on specified port
* Displays all images in HTML page using `file://` scheme for performance
* Click on each to see a full-window version of the image
* Mark images with a tick or a cross, or unmark them
* Choose actions for each:
    * `rm`
    * move to "Trash" subfolder
    * Create a ZIP file in the same folder
    * Copy to path
    * Add a prefix
    * Rename using a regex

Specifically, unlike previous thought, we will not initially seek to implement folder navigation, and "server" side storage of marker data. All marker data is in-session only in MVP.

## Suggested tech

* Flask for processing server listening
* jQuery to manage display objects
* Ajax for sending backend queries - do not navigate away

## Triage interface

When launched, the server can be accessed on http://localhost:3080

The interface provides:

* Thumbnail listing
* Ability to click to enlarge / return to thumbnails view
* Ability to add/remove any markers to individual files
* Ability to run an action on items marked by a specific marker
* Clear all markers
* Restrict thumbnails to those marked by one marker

Nice to have:

* Keyboard shortcuts:
    * `/` - clear markers on selected item(s)
    * `<space>` - display/exit one selected item in large view
    * `<Esc>` - activate/deactivate actions box
    * Any alpha-numeric key - mark/unmark with marker (case-insensitive)
    * (Arrow key left/right) - move one previous/next
        * (with shift key) - move 5 previous/next