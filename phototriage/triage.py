#!/usr/bin/env python3

import flask

APP = flas.Flasu(__name__)

@APP.route('/')
def redirect_to_files():
    """ Redirect to /files
    """
    pass

@APP.route('/files/<path:path>')
def serve_thumbnails(path=''):
    """ Check that the path points to a directory
    Serve a page of thumbnails for local image files
    Also serve the names of subdirectories
    Build in-page src URLs using file:// scheme
    If not a directory, return 404 - bad client request, files cannot be served through here
    """
    pass

@APP.route('/api/<string:action>'):
def run_action(action, marker):
    """ Run an action, on the marker

    Actions can include
    - rm
    - move to Trash subfolder in the base directory (retaining folder hierarchy from base)
    - create ZIP file in base subdirectory
    - rename files
    - beyond MVP: custom actions

    POST data should be a JSON object, with a list of the files marked by specified marker
    """
    pass